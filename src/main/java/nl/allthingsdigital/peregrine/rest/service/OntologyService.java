/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.peregrine.rest.service;

import org.erasmusmc.data_mining.ontology.api.Concept;
import org.erasmusmc.data_mining.ontology.api.Ontology;
import org.erasmusmc.data_mining.ontology.api.Term;
import org.erasmusmc.data_mining.ontology.common.TermIdImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * RESTful Controller exposing the Ontology service to query concepts & terms.
 * 
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
@Service
@RestController
@RequestMapping("/ontology")
public class OntologyService {
    @Autowired
    private Ontology ontology;

    @RequestMapping("/get/{concept}")
    public Concept getConcept(@PathVariable("concept") final Integer conceptId) {
        return ontology.getConcept(conceptId);
    }

    @RequestMapping("/get/{concept}/{term}")
    public Term getTerm(@PathVariable("concept") final Integer conceptId, @PathVariable("term") final Integer termId) {
        Term.Id term = new TermIdImpl(termId, conceptId);
        return ontology.getTerm(term);
    }
}
