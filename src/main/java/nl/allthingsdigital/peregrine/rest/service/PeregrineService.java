/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.peregrine.rest.service;

import java.util.List;
import org.erasmusmc.data_mining.ontology.api.Language;
import org.erasmusmc.data_mining.peregrine.api.IndexingDisambiguationResult;
import org.erasmusmc.data_mining.peregrine.api.IndexingResult;
import org.erasmusmc.data_mining.peregrine.api.Peregrine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author david
 */
@RestController
@RequestMapping("/peregrine")
public class PeregrineService {

    @Autowired
    private Peregrine peregrine;

    @RequestMapping("/index/verbose")
    public List<IndexingDisambiguationResult> indexVerbose(@RequestParam("text") final String text,
            @RequestParam(value = "lang", defaultValue = "EN") final Language language) {
        return peregrine.indexVerbose(text, language);

    }

    @RequestMapping("/index")
    public List<IndexingResult> index(@RequestParam("text") final String text,
            @RequestParam(value = "lang", defaultValue = "EN") final Language language) {

        return peregrine.index(text, language);
    }

    @RequestMapping("/disambiguate/verbose")
    public List<IndexingDisambiguationResult> disambiguateVerbose(@RequestParam("text") final String text,
            @RequestParam(value = "lang", defaultValue = "EN") final Language language) {

        return peregrine.indexAndDisambiguateVerbose(text, language);
    }

    @RequestMapping("/disambiguate")
    public List<IndexingResult> disambiguate(@RequestParam("text") final String text,
            @RequestParam(value = "lang", defaultValue = "EN") final Language language) {

        return peregrine.indexAndDisambiguate(text, language);
    }
}
