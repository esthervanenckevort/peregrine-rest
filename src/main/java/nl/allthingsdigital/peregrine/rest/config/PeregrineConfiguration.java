/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.peregrine.rest.config;

import java.io.IOException;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import nl.allthingsdigital.peregrine.rest.util.SnowballStemmerFactory;
import org.erasmusmc.data_mining.ontology.api.Language;
import org.erasmusmc.data_mining.ontology.api.Ontology;
import org.erasmusmc.data_mining.peregrine.api.Peregrine;
import org.erasmusmc.data_mining.peregrine.disambiguator.api.DisambiguationDecisionMaker;
import org.erasmusmc.data_mining.peregrine.disambiguator.api.Disambiguator;
import org.erasmusmc.data_mining.peregrine.disambiguator.api.RuleDisambiguator;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.ThresholdDisambiguationDecisionMakerImpl;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.rule_based.LooseDisambiguator;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.rule_based.StrictDisambiguator;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.rule_based.TypeDisambiguatorImpl;
import org.erasmusmc.data_mining.peregrine.impl.hash.PeregrineImpl;
import org.erasmusmc.data_mining.peregrine.normalizer.api.Normalizer;
import org.erasmusmc.data_mining.peregrine.normalizer.api.NormalizerFactory;
import org.erasmusmc.data_mining.peregrine.normalizer.impl.LVGNormalizer;
import org.erasmusmc.data_mining.peregrine.normalizer.impl.NormalizerFactoryImpl;
import org.erasmusmc.data_mining.peregrine.normalizer.impl.SnowballNormalizer;
import org.erasmusmc.data_mining.peregrine.tokenizer.api.Tokenizer;
import org.erasmusmc.data_mining.peregrine.tokenizer.api.TokenizerFactory;
import org.erasmusmc.data_mining.peregrine.tokenizer.impl.ChemicalSBDtokenizer;
import org.erasmusmc.data_mining.peregrine.tokenizer.impl.ChineseTokenizer;
import org.erasmusmc.data_mining.peregrine.tokenizer.impl.SubSentenceTokenizer;
import org.erasmusmc.data_mining.peregrine.tokenizer.impl.TokenizerFactoryImpl;
import org.erasmusmc.data_mining.peregrine.tokenizer.impl.UMLSGeneChemTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tartarus.snowball.SnowballStemmer;
import nl.allthingsdigital.peregrine.rest.config.NormalizerConfiguration.NormalizerConfig;
import nl.allthingsdigital.peregrine.rest.config.TokenizerConfiguration.TokenizerConfig;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.rule_based.rule.HasKeywordRule;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.rule_based.rule.HasSynonymRule;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.rule_based.rule.IsComplexRule;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.rule_based.rule.IsNotHomonymRule;
import org.erasmusmc.data_mining.peregrine.disambiguator.impl.rule_based.rule.IsPreferredTermRule;

/**
 *
 * @author david
 */
@Configuration
public class PeregrineConfiguration {

    @Value("${peregrine.defaultLanguage:EN}")
    private Language defaultLanguage;

    @Value("${peregrine.languages:EN}")
    private List<Language> languages;

    @Value("${peregrine.decisionMaker.disambiguationMinimalWeight:50}")
    private byte disambiguationMinimalWeight;
    @Value("${peregrine.decisionMaker.disambiguationAlwaysAcceptedWeight:80}")
    private byte disambiguationAlwaysAcceptedWeight;
    @Value("${peregrine.hasSynonymRule.maxSynonymDistance:100}")
    private int maxSynonymDistance;
    @Value("${peregrine.hasSynonymRule.minSynonymWeight:75}")
    private byte minSynonymWeight;
    @Value("${peregrine.hasSynonymRule.maxSynonymWeight:80}")
    private byte maxSynonymWeight;
    @Value("${peregrine.isComplexRule.maxTermLength:6}")
    private int maxTermLength;
    @Value("${peregrine.isComplexRule.minTermLength:3}")
    private int minTermLength;
    @Value("${peregrine.isComplexRule.minTermNumbers:1}")
    private int minTermNumbers;
    @Value("${peregrine.isComplexRule.minTermLetters:1}")
    private int minTermLetters;
    @Value("${peregrine.hasKeywordRule.maxKeywordDistance:50}")
    private int maxKeywordDistance;
    @Value("${peregrine.hasKeywordRule.minKeywordWeight:70}")
    private byte minKeywordWeight;
    @Value("${peregrine.hasKeywordRule.maxKeywordWeight:75}")
    private byte maxKeywordWeight;

    @Bean
    public NormalizerFactory getNormalizerFactory(
            final NormalizerConfiguration normalizerConfiguration)
            throws IOException {
        
        final Map<Language, Normalizer> map = new EnumMap<>(Language.class);
        for (NormalizerConfig normalizer : normalizerConfiguration.getNormalizers()) {
            Language language = normalizer.getLanguage();
            switch (normalizer.getType()) {
                case LVG:
                    map.put(language, LVGNormalizer.normalizerFactory());
                    break;
                case SNOWBALL:
                    final SnowballStemmer stemmer = SnowballStemmerFactory.create(language);
                    map.put(language, new SnowballNormalizer(stemmer));
                    break;
                default:
                    throw new RuntimeException("Unexpected normalizer type");
            }
        }
        if (map.get(defaultLanguage) == null) {
            throw new RuntimeException("No normalizer defined for language " + defaultLanguage);
        }
        map.put(Language.DEFAULT, map.get(defaultLanguage));
        return new NormalizerFactoryImpl(map);
    }

    @Bean
    public Peregrine getPeregrine(final Ontology ontology,
            final TokenizerFactory tokenizerFactory,
            final NormalizerFactory normalizerFactory,
            final Disambiguator disambiguator,
            final DisambiguationDecisionMaker disambiguationDecisionMaker) {

        return new PeregrineImpl(ontology, tokenizerFactory, normalizerFactory,
                disambiguator, disambiguationDecisionMaker, languages);
    }

    @Bean
    public TokenizerFactory getTokenizerFactory(
            final TokenizerConfiguration config) throws IOException {

        final Map<Language, Tokenizer> tokenizers = new EnumMap<>(Language.class);
        for (TokenizerConfig tokenizer : config.getTokenizers()) {
            final Language language = tokenizer.getLanguage();
            switch (tokenizer.getType()) {
                case CHEMICAL:
                    tokenizers.put(language, new ChemicalSBDtokenizer());
                    break;
                case CHINESE:
                    tokenizers.put(language, new ChineseTokenizer());
                    break;
                case GENE_CHEM:
                    tokenizers.put(language, new UMLSGeneChemTokenizer());
                    break;
                case SUBSENTENCE:
                    tokenizers.put(language, new SubSentenceTokenizer());
                    break;
                default:
                    throw new RuntimeException("Unexpected tokenizer type");
            }
        }
        if (tokenizers.get(defaultLanguage) == null) {
            throw new RuntimeException("No tokenizer defined for language "
                    + defaultLanguage);
        }
        tokenizers.put(Language.DEFAULT, tokenizers.get(defaultLanguage));
        return new TokenizerFactoryImpl(tokenizers);
    }

    @Bean
    public Disambiguator getDisambiguator() {
        final IsNotHomonymRule notHomonymRule = new IsNotHomonymRule();
        final IsPreferredTermRule preferredTermRule = new IsPreferredTermRule();

        final HasSynonymRule hasSynonymRule = new HasSynonymRule();
        hasSynonymRule.setMaxSynonymDistance(maxSynonymDistance);
        hasSynonymRule.setMaxSynonymWeight(maxSynonymWeight);
        hasSynonymRule.setMinSynonymWeight(minSynonymWeight);

        final IsComplexRule complexRule = new IsComplexRule();
        complexRule.setMinTermLength(minTermLength);
        complexRule.setMaxTermLength(maxTermLength);
        complexRule.setMinTermLetters(minTermLetters);
        complexRule.setMinTermNumbers(minTermNumbers);

        final HasKeywordRule hasKeywordRule = new HasKeywordRule();
        hasKeywordRule.setMaxKeywordDistance(maxKeywordDistance);
        hasKeywordRule.setMinKeywordWeight(minKeywordWeight);
        hasKeywordRule.setMaxKeywordWeight(maxKeywordWeight);

        final StrictDisambiguator strict = new StrictDisambiguator(
                notHomonymRule, preferredTermRule, complexRule,
                hasSynonymRule, hasKeywordRule);

        final LooseDisambiguator loose = new LooseDisambiguator(notHomonymRule,
                preferredTermRule, hasSynonymRule);

        final RuleDisambiguator[] disambiguators = {strict, loose};
        return new TypeDisambiguatorImpl(disambiguators);
    }

    @Bean
    public DisambiguationDecisionMaker getDisambiguationDecisionMaker() {
        ThresholdDisambiguationDecisionMakerImpl decisionMaker;
        decisionMaker = new ThresholdDisambiguationDecisionMakerImpl();
        decisionMaker.setDisambiguationAlwaysAcceptedWeight(disambiguationAlwaysAcceptedWeight);
        decisionMaker.setDisambiguationMinimalWeight(disambiguationMinimalWeight);
        return decisionMaker;
    }
}
