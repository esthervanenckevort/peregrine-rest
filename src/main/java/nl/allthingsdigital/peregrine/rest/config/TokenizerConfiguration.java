/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.peregrine.rest.config;

import java.util.ArrayList;
import java.util.List;
import nl.allthingsdigital.peregrine.rest.util.TokenizerType;
import org.erasmusmc.data_mining.ontology.api.Language;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author david
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "peregrine")
public class TokenizerConfiguration {
    private final List<TokenizerConfig> tokenizers = new ArrayList<>();

    public List<TokenizerConfig> getTokenizers() {
        return tokenizers;
    }

    public static class TokenizerConfig {

        private TokenizerType type;
        private Language language;

        public Language getLanguage() {
            return language;
        }

        public void setLanguage(Language language) {
            this.language = language;
        }

        public TokenizerType getType() {
            return type;
        }

        public void setType(TokenizerType type) {
            this.type = type;
        }
    }


}
