/*
 * Copyright 2015 <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.peregrine.rest.config;

import java.io.IOException;
import net.sf.ehcache.CacheManager;
import org.erasmusmc.data_mining.ontology.api.Ontology;
import org.erasmusmc.data_mining.ontology.impl.cache.CachedOntologyImpl;
import org.erasmusmc.data_mining.ontology.impl.skos.SkosOntologyImpl;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.Sail;
import org.openrdf.sail.memory.MemoryStore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@Configuration
@Profile("skos")
public class SkosOntologyConfiguration {

    @Value("${ontology}")
    private String ontologyFile;
    @Value("${cache.configuration}")
    private Resource cacheConfiguration;

    @Bean
    public EhCacheManagerFactoryBean cacheFactory() {
        EhCacheManagerFactoryBean factory = new EhCacheManagerFactoryBean();
        factory.setConfigLocation(cacheConfiguration);
        return factory;
    }

    @Bean
    public Sail getStore() {
        return new MemoryStore();
    }

    @Bean
    public Repository getRepository(final Sail sail)
            throws RepositoryException {

        Repository repository = new SailRepository(sail);
        repository.initialize();
        return repository;
    }

    @Bean
    public Ontology getOntology(final CacheManager cacheManager,
            final Repository repository)
            throws RepositoryException, IOException, RDFParseException {

        final Ontology ontology;
        ontology = new SkosOntologyImpl(repository, ontologyFile);
        return new CachedOntologyImpl(ontology, cacheManager);
    }
}
