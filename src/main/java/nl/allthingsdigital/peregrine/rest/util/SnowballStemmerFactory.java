/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.peregrine.rest.util;

import org.erasmusmc.data_mining.ontology.api.Language;
import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.danishStemmer;
import org.tartarus.snowball.ext.dutchStemmer;
import org.tartarus.snowball.ext.englishStemmer;
import org.tartarus.snowball.ext.finnishStemmer;
import org.tartarus.snowball.ext.frenchStemmer;
import org.tartarus.snowball.ext.germanStemmer;
import org.tartarus.snowball.ext.hungarianStemmer;
import org.tartarus.snowball.ext.italianStemmer;
import org.tartarus.snowball.ext.norwegianStemmer;
import org.tartarus.snowball.ext.portugueseStemmer;
import org.tartarus.snowball.ext.romanianStemmer;
import org.tartarus.snowball.ext.russianStemmer;
import org.tartarus.snowball.ext.spanishStemmer;
import org.tartarus.snowball.ext.swedishStemmer;
import org.tartarus.snowball.ext.turkishStemmer;

/**
 *
 * @author david
 */
public class SnowballStemmerFactory {

    public static SnowballStemmer create(Language language) {
        SnowballStemmer stemmer;
        switch (language) {
            case DA:
                stemmer = new danishStemmer();
                break;
            case DE:
                stemmer = new germanStemmer();
                break;
            case EN:
                stemmer = new englishStemmer();
                break;
            case ES:
                stemmer = new spanishStemmer();
                break;
            case FI:
                stemmer = new finnishStemmer();
                break;
            case FR:
                stemmer = new frenchStemmer();
                break;
            case HU:
                stemmer = new hungarianStemmer();
                break;
            case IT:
                stemmer = new italianStemmer();
                break;
            case NL:
                stemmer = new dutchStemmer();
                break;
            case NO:
                stemmer = new norwegianStemmer();
                break;
            case PT:
                stemmer = new portugueseStemmer();
                break;
            case RO:
                stemmer = new romanianStemmer();
                break;
            case RU:
                stemmer = new russianStemmer();
                break;
            case SV:
                stemmer = new swedishStemmer();
                break;
            case TR:
                stemmer = new turkishStemmer();
                break;
            default:
                throw new RuntimeException("Unsupported language for the Snowball stemmer");
        }
        return stemmer;
    }

}
