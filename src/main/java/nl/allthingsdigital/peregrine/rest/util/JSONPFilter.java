/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.peregrine.rest.util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
@Component
public class JSONPFilter implements Filter {

    private static final String CALLBACK_PARAM = "callback";
    private static final String CALLBACK_VALIDATION_REGEX = "^\\w{1,16}$";
    private static final String CONTENT_TYPE_JAVASCRIPT = "application/javascript;charset=UTF-8";

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res,
            final FilterChain chain) throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) req;
        final String callback = request.getParameter(CALLBACK_PARAM);

        if (StringUtils.isNotBlank(callback)) {

            final HttpServletResponse response = (HttpServletResponse) res;
            final HttpMethod method = HttpMethod.valueOf(request.getMethod());
            // Validate preconditions:
            // - Only GET requests
            // - Callback method name consists of 1..16 alphanumeric chars
            if (!callback.matches(CALLBACK_VALIDATION_REGEX)
                    || !HttpMethod.GET.equals(method)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                chain.doFilter(req, res);
            } else {
                final JSONPResponse wrapper = new JSONPResponse(response);
                chain.doFilter(request, wrapper);

                response.setContentType(CONTENT_TYPE_JAVASCRIPT);
                try (ServletOutputStream output = response.getOutputStream()) {
                    output.print(callback + "(");
                    output.write(wrapper.getData());
                    output.print(");");
                }
            }
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void init(final FilterConfig fc) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}
