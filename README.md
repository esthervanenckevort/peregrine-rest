# README #

The Peregrine-REST application provides a self-contained and easy to setup
Peregrine RESTful Web Service.

## How do I get set up? ##

### As a user ###
Peregrine-REST is a Java application written in Java 7. To run the application
you need a Java Runtime Environment  (JRE) version 7 or above.

As an end-user you should download a JAR file containing the program from the
project website, since this will provide the fastest way to get up and running.

Once you have downloaded the JAR file you can run the program with the following
command:

```
#!bash
java -jar path/to/jar/file --ontology=path/to/ontology/file

```
### As a developer ###

To build the application you need the full Java Development Kit (JDK) version 7
or above. The project uses Apache Maven to manage the builds.

The program is depending on Peregrine, which is not in the Maven Central
repositories, but can be found in the DTL maven repository.

https://ci.dtls.nl/nexus/content/groups/public/

### Configuration ###

The application can be configured with command line options or with a
configuration file called application.properties.

The application comes configured with reasonable defaults, which means that in
a typical use-case you only need to specify your ontology file.

#### Ontology ####

#### Language support ####
Language support consists of two key elements:

1. Tokenizer configuration
2. Normalizer configuration

Peregrine can support multiple languages with each their own tokenizer and
normalizer.

##### Tokenizers #####
Tokenizers are configured with the tokenizer lemma, which defines the list of
tokenizers. Per tokenizer the type and the language to which it applies must
be specified.

```
peregrine.tokenizers[0].language=EN
peregrine.tokenizers[0].type=GENE_CHEM
peregrine.tokenizers[1].language=ZH
peregrine.tokenizers[1].type=CHINESE
```

The language is an uppercase ISO 639-1 language code, currently acceptable
values are:

- DA: Danish
- DE: German
- EN: English
- ES: Spanish
- FI: Finnish
- FR: French
- HU: Hungarian
- IT: Italian
- NL: Dutch
- NO: Norwegian
- PT: Portuguese
- RO: Romanian
- RU: Russian
- SV: Swedish
- TR: Turkish
- ZH: Chinese

The type takes the following options as cceptable values:

- CHEMICAL
- CHINESE
- GENE_CHEM
- SUBSENTENCE

##### Normalizers #####
Normalizers are configured with the normalizers lemma, which defines a list
of normalizers. Per normalizer the type and the language to which it applies
must be specified.

```
peregrine.normalizers[0].language=EN
peregrine.normalizers[0].type=LVG
```

The language is an uppercase ISO 639-1 language code, currently acceptable
values are the same as for the tokenizers.

The type takes the following options as acceptable values:

- LVG (English only)
- SNOWBALL (All languages except Chinese)

The application comes with an embedded version of LVG, no further configuration
is necessary to use LVG.

#### Disambiguation fine-tuning ####
Disambiguation can be fine-tuned with the following options:

```
peregrine.decisionMaker.disambiguationMinimalWeight=50
peregrine.decisionMaker.disambiguationAlwaysAcceptedWeight=80
peregrine.hasSynonymRule.maxSynonymDistance=100
peregrine.hasSynonymRule.minSynonymWeight=75
peregrine.hasSynonymRule.maxSynonymWeight=80
peregrine.isComplexRule.maxTermLength=6
peregrine.isComplexRule.minTermLength=3
peregrine.isComplexRule.minTermNumbers=1
peregrine.isComplexRule.minTermLetters=1
peregrine.hasKeywordRule.maxKeywordDistance=50
peregrine.hasKeywordRule.minKeywordWeight=70
peregrine.hasKeywordRule.maxKeywordWeight=75
```
